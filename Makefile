include config.mk

VPATH=$(topdir)

mk=make -C $(topdir)/linux O=$(builddir) ARCH=$(arch)

# Read KERNELRELEASE from include/config/kernel.release (if it exists)
KERNELRELEASE = $(shell cat include/config/kernel.release 2> /dev/null)

all:
	+$(mk) bzImage modules

clean:
	+$(mk) clean

install: buildapi-install-runtime buildapi-install-devel

buildapi-install-runtime:
	mkdir -p $(DESTDIR)/boot
	install -m 0644 arch/$(arch)/boot/bzImage $(DESTDIR)/boot/vmlinuz-$(KERNELRELEASE)
	install -m 0644 System.map $(DESTDIR)/boot/System.map-$(KERNELRELEASE)
	+$(mk) INSTALL_MOD_PATH=$(DESTDIR)/usr INSTALL_MOD_STRIP=1 modules_install

buildapi-install-devel:
	+$(mk) INSTALL_HDR_PATH=$(DESTDIR)/usr headers_install

ifeq ($(arch),x86_64)
CONFIG_FILES =					\
	config/x86_64-generic.config		\
	config/x86-generic.config		\
	config/generic.config			\
	config/nodebug.config
else
$(error $(arch) does not have config files)
endif

merged.config: $(CONFIG_FILES) Makefile $(topdir)/manage-configs
	if python $(topdir)/manage-configs --srcdir=$(topdir) merge $(CONFIG_FILES) > $@.tmp ; then \
	    mv $@.tmp $@ ; \
        else \
            rm $@.tmp ; exit 1 ; \
	fi

defconfig:
	rm -f .config
	$(mk) defconfig
	mv .config config.default

.config: merged.config
	cp merged.config .config
	$(mk) olddefconfig

configure: .config

update-configs: merged.config .config $(topdir)/update-config.py
	for cf in $(CONFIG_FILES) ; do \
		$(topdir)/update-config.py merged.config .config $(topdir)/$$cf ; \
	done

.PHONY: all clean defconfig install buildapi-install-runtime buildapi-install-devel configure update-configs
